# 实验三：全球新型冠状病毒实时数据统计应用程序的设计与实现

## 一、实验目的  
#### 1、掌握使用Spring框架自带的RestTemplate工具类爬取网络数据；  
#### 2、掌握使用Spring框架自带的计划任务功能；  
#### 3、掌握使用Apache Commons CSV组件解释CSV文件；  
#### 4、掌握Java 8的Stream API处理集合类型数据；  
#### 5、了解使用模板引擎或前端框架展示数据。  

## 二、实验环境  
#### 1、JDK 1.8或更高版本  
#### 2、Maven 3.6+  
#### 3、IntelliJ IDEA  
#### 4、commons-csv 1.8+  

## 三、实验任务  
#### 1、通过IntelliJ IDEA的Spring Initializr向导创建Spring Boot项目。  

#### 2、添加功能模块：spring MVC、lombok、commons-csv等。  
&ensp;&ensp;&ensp;&ensp;推荐使用commons-csv组件处理csv文件：  
<div align=center><img src="https://images.gitee.com/uploads/images/2020/1118/155752_af07ee59_8032586.png"></div>  

&ensp;&ensp;&ensp;&ensp;[Apache Commons CSV 官方用户指南:](https://commons.apache.org/proper/commons-csv/user-guide.html)  

<br/></br>
**答：**  
<div align=center>
<img src="https://images.gitee.com/uploads/images/2020/1118/163600_bdd63cd8_8032586.png">  
<img src="https://images.gitee.com/uploads/images/2020/1118/163647_f71809a9_8032586.png">  
</div>  

#### 3、爬取全球冠状病毒实时统计数据。（Java，Spring）  
&ensp;&ensp;&ensp;&ensp;在Github上，有一个由约翰·霍普金斯大学系统科学与工程中心（JHU CSSE）运营的2020年新型冠状病毒可视化仪表板的数据仓库，大家可以从该仓库中爬取全球新型冠状病毒最新的统计数据。  
&ensp;&ensp;&ensp;&ensp;[Github仓库地址](https://github.com/CSSEGISandData/COVID-19)  
&ensp;&ensp;&ensp;&ensp;该仓库会把全球新型冠状病毒最新的统计数据汇总到一个csv文件上，大家在爬取数据时，获取这个csv文件即可.  
&ensp;&ensp;&ensp;&ensp;[Github仓库的csv文件地址](https://github.com/CSSEGISandData/COVID-19/blob/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv)  
&ensp;&ensp;&ensp;&ensp;由于国内网络访问限制的原因，我们不能正常访问Github。老师已经克隆整个仓库，并上传到Gitee，方便大家进行实验.  
&ensp;&ensp;&ensp;&ensp;[克隆的Gitee仓库地址](https://gitee.com/dgut-sai/COVID-19)  
&ensp;&ensp;&ensp;&ensp;[克隆的Gitee仓库的csv文件地址](https://gitee.com/dgut-sai/COVID-19/blob/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv)  

#### 4、使用Spring框架自带的RestTemplate工具类爬取数据。
&ensp;&ensp;&ensp;&ensp;RestTemplate采用同步方式执行 HTTP 请求的类，底层使用 JDK 原生 HttpURLConnection API ，或者 HttpComponents等其他 HTTP 客户端请求类库。  
&ensp;&ensp;&ensp;&ensp;RestTemplate 工具类提供模板化的方法让开发者能更简单地发送 HTTP 请求。  
&ensp;&ensp;&ensp;&ensp;RestTemplate 就是 Spring框架 封装的处理同步 HTTP 请求的工具类。  
&ensp;&ensp;&ensp;&ensp;[RestTemplate 官方使用指南](https://docs.spring.io/spring/docs/current/spring-framework-reference/integration.html#rest-client-access)  
<div align=center><img src="https://images.gitee.com/uploads/images/2020/1118/161106_27f7ef2f_8032586.png"></div>  

&ensp;&ensp;&ensp;&ensp;另外，大家可以用 [WebClient](https://docs.spring.io/spring-framework/docs/current/reference/html/web.html#webmvc-webclient)。  

&ensp;&ensp;&ensp;&ensp;在本实验中，我们可以使用 RestTemplate 工具类获取上述的csv文件。  
&ensp;&ensp;&ensp;&ensp;如果直接使用HTTP客户端请求Gitee上的文件，会报403的错误，但使用浏览器访问是正常的。经过分析，我们使用HTTP客户端请求Gitee上的文件时，需要设置一个请求头部User-Agent，否则会报403异常。  
<div align=center><img src="https://images.gitee.com/uploads/images/2020/1118/160624_396865bc_8032586.png"></div>  
&ensp;&ensp;&ensp;&ensp;当我们使用 RestTemplate 工具类时，可以如下图所示设置请求头部并请求数据：  
<div align=center><img src="https://images.gitee.com/uploads/images/2020/1118/160701_14a2dfda_8032586.png"></div>  
&ensp;&ensp;&ensp;&ensp;如上图所示，我们先实例化一个RequestEntity对象，再通过RestTemplate的exchange方法获取csv文件，这个文件的数据会封装到一个Resource对象中。我们可以通过Resource对象的getInputStream方法获取csv文件的输入流。  

<br/></br>
**答：**  
<div align=center>
<img src="https://images.gitee.com/uploads/images/2020/1118/163828_5938e741_8032586.png">  
<img src="https://images.gitee.com/uploads/images/2020/1118/163847_19928080_8032586.png">  
</div>  

#### 5、分析csv文件的数据结构，定义model类。  
&ensp;&ensp;&ensp;&ensp;如下图所示，这是一个model类的样例：  

<div align=center><img src="https://images.gitee.com/uploads/images/2020/1118/161233_a36bfc6e_8032586.png"></div>  

<br/></br>
**答：**  
<div align=center>
<img src="https://images.gitee.com/uploads/images/2020/1118/163950_874b60f3_8032586.png">  
</div>  

#### 6、使用Apache Commons CSV组件解释CSV文件。  
&ensp;&ensp;&ensp;&ensp;[CSV文件WIKI](https://docs.qq.com/scenario/link.html?url=https%3A%2F%2Fzh.wikipedia.org%2Fwiki%2F%25E9%2580%2597%25E5%258F%25B7%25E5%2588%2586%25E9%259A%2594%25E5%2580%25BC&pid=300000000$RQeslpiqqOPH&cid=5856113)  
&ensp;&ensp;&ensp;&ensp;CSV文件是由任意数目的记录组成的，记录间以某种换行符分隔；每条记录由字段组成，字段间的分隔符是其它字符或字符串，最常见的是逗号或制表符。CSV文件是以纯文本形式存储表格数据（数字和文本）。  
&ensp;&ensp;&ensp;&ensp;为了方便应用程序处理，我们通常会把csv文件的每条记录转换为一个modle类的对象，即上一步中我们定义的model类。  
&ensp;&ensp;&ensp;&ensp;把所有的model类的对象组织为一个集合，如：List 。最终，这个csv文件会转换为一个List对象。然后我们可以根据实际的业务逻辑，访问这个List即可。  
&ensp;&ensp;&ensp;&ensp;我们可以定义一个Service组件处理csv文件的转换逻辑，并把最终转换后的List对象赋值给这个Service组件的成员属性，以便应用程序访问这个List对象。  
&ensp;&ensp;&ensp;&ensp;使用Apache Commons CSV组件，我们可以很方便地解释CSV文件，详细使用方法可以浏览[官方的用户指南](https://commons.apache.org/proper/commons-csv/user-guide.html)。  
&ensp;&ensp;&ensp;&ensp;**提示：**  
&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;record.get("Province/State")	可以获取记录某个字段名的值；  
&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;record.get("11/8/20")  可以获取某个地区的指定时间的病毒数值。  
&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;record.get(record.size() - 1)	可以获取记录最后一个字段的值。  

**答：**  
<div align=center>
<img src="https://images.gitee.com/uploads/images/2020/1118/165009_0bcf5837_8032586.png">  
<img src="https://images.gitee.com/uploads/images/2020/1118/165100_64098045_8032586.png">  
</div>

#### 7、使用Spring框架自带的计划任务功能定时更新统计数据。  
&ensp;&ensp;&ensp;&ensp;[Scheduling Tasks计划任务官方使用指南](https://spring.io/guides/gs/scheduling-tasks/)  
&ensp;&ensp;&ensp;&ensp;要实现计划任务，首先通过在配置类上添加 @EnableScheduling 注解来开启对计划任务的支持，然后在要执行计划任务的方法上注解 @Scheduled，声明这是一个计划任务。
&ensp;&ensp;&ensp;&ensp;其中 @Scheduled 注解中有以下几个参数：
&ensp;&ensp;&ensp;&ensp;1、cron：cron表示式，指定任务在特定时间执行；
&ensp;&ensp;&ensp;&ensp;2、fixedDelay：表示上一次任务执行完成后多久再次执行，参数类型这long，单位ms；  
&ensp;&ensp;&ensp;&ensp;3、fixedDelayString：与fixedDelay含义一样，只是参数类型变为String；  
&ensp;&ensp;&ensp;&ensp;4、fixedRate：表示按一定的频率执行任务，参数类型为long，单位ms；  
&ensp;&ensp;&ensp;&ensp;5、fixedRateString: 与fixedRate的含义一样，只是将参数类型变为String；  
&ensp;&ensp;&ensp;&ensp;6、initialDelay：表示延迟多久后再第一次執行任務，参数类型为long，單位ms；  
&ensp;&ensp;&ensp;&ensp;7、initialDelayString：与initialDelay的含义一样，只是参数类型变为String；  
&ensp;&ensp;&ensp;&ensp;8、zone：时区，默认为当前时区，一般不需要设置。  
&ensp;&ensp;&ensp;&ensp;如下图所示，例子中配置了每天凌晨1点执行定时任务，更新统计数据：  
<div align=center><img src="https://images.gitee.com/uploads/images/2020/1118/161953_5b856932_8032586.png"></div>  
&ensp;&ensp;&ensp;&ensp;另外，可以在cron参数中使用${...}占位符，读取属性文件中的自定义属性配置cron参数，如下图所示：  
<div align=center><img src="https://images.gitee.com/uploads/images/2020/1118/162040_243173e5_8032586.png"></div>  
&ensp;&ensp;&ensp;&ensp;在属性文件中自定义属性配置cron表达式，如下图所示：  
<div align=center><img src="https://images.gitee.com/uploads/images/2020/1118/162113_94f62bd1_8032586.png"></div>  
&ensp;&ensp;&ensp;&ensp;*当cron="-"时，表示关闭计划任务。*  

&ensp;&ensp;&ensp;&ensp;**注意：**在Spring Boot应用程序中，Spring框架自带的计划任务默认是在单线程中执行的，这是因为Spring Boot在初始化计划任务线程池的时候，默认的线程池大小是 1 ，也就是说只有一个线程来执行所有计划任务，那么当有一个计划任务要耗费很长时间执行时，其它的计划任务即使到了执行时间点也只能继续等待。  
&ensp;&ensp;&ensp;&ensp;我们可以通过属性文件设置计划任务线程池的大小，实现并行执行计划任务，如下图所示：  
<div align=center><img src="https://images.gitee.com/uploads/images/2020/1118/162335_20a05c9d_8032586.png"></div>  
&ensp;&ensp;&ensp;&ensp;推荐大家阅读关于计划任务功能的Spring Boot自动配置类，如下图所示：  
<div align=center><img src="https://images.gitee.com/uploads/images/2020/1118/162404_7a529fdd_8032586.png"></div>  

<br/></br>
**答：**  
<div align=center>
<img src="https://images.gitee.com/uploads/images/2020/1118/165622_3d68b211_8032586.png">  
<img src="https://images.gitee.com/uploads/images/2020/1118/165657_75a19c17_8032586.png">  
</div>

#### 8、要确保应用程序启动时，获取一次统计数据。  
&ensp;&ensp;&ensp;&ensp;提示：  
&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;1、InitializingBean；  
&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;2、@PostConstruct;  
&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;3、CommandLineRunner。  

**答：**  
<div align=center>
<img src="https://images.gitee.com/uploads/images/2020/1118/185157_1d49aab4_8032586.png">  
<img src="https://images.gitee.com/uploads/images/2020/1118/185121_59621c73_8032586.png">  
</div>

#### 9、单元测试。  
&ensp;&ensp;&ensp;&ensp;**必须编写单元测试。**  
&ensp;&ensp;&ensp;&ensp;*所有控制器 与 Service 组件，必须写单元测试用例进行测试。*  
&ensp;&ensp;&ensp;&ensp;如下图所示，例子中演示了如何对一个Service组件进行单元测试。  
<div align=center><img src="https://images.gitee.com/uploads/images/2020/1118/162708_775616d3_8032586.png"></div>  
&ensp;&ensp;&ensp;&ensp;提示：掌握如何配置@SpringBootTest注解的属性，从而构造一个精简的单元测试环境。  

<br/></br>
**答：**  
<div align=center>
<img src="https://images.gitee.com/uploads/images/2020/1118/170429_0177b8db_8032586.png">  
<img src="https://images.gitee.com/uploads/images/2020/1118/170556_dfa062b2_8032586.png">  
</div>

#### 10、定义Cotroller控制器。  
&ensp;&ensp;&ensp;&ensp;我们需要定义一个控制器，用于返回数据给前端展示。  
&ensp;&ensp;&ensp;&ensp;**控制器必须支持查询功能。如：可以选择查询某个地区或国家的新型冠状病毒实时统计数据**  
&ensp;&ensp;&ensp;&ensp;**提示：**  
&ensp;&ensp;&ensp;&ensp;1、 使用Java 8的Stream API可以很方便地访问集合类型对象，如：List ；  
&ensp;&ensp;&ensp;&ensp;2、 parallelStream支持并行流，可以大大提升访问性能；  
&ensp;&ensp;&ensp;&ensp;3、 filter方法支持设置过滤条件；  
&ensp;&ensp;&ensp;&ensp;4、 Collectors.toUnmodifiableList()收集为不可变的List集合。  
<br/></br>
**答：**  
<div align=center>
<img src="https://images.gitee.com/uploads/images/2020/1118/184556_cc5eba0a_8032586.png">  
<img src="https://images.gitee.com/uploads/images/2020/1118/170725_2904d7ca_8032586.png">  
<img src="https://images.gitee.com/uploads/images/2020/1118/170740_ba7bc97a_8032586.png">  
</div>

#### 11、定义前端数据展示页面。  
&ensp;&ensp;&ensp;&ensp;可以使用 任何视图引擎 或 任何前端框架 构建前端数据展示。  
&ensp;&ensp;&ensp;&ensp;要求：界面美观、整洁，支持查询功能。如果页面包含图表展示功能，加15分。  
<br/></br>
**答：**  
<div align=center>
&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;全球疫情：  
<img src="https://images.gitee.com/uploads/images/2020/1118/171105_3f5c6102_8032586.png">  
<img src="https://images.gitee.com/uploads/images/2020/1118/171125_9918849e_8032586.png">  
<img src="https://images.gitee.com/uploads/images/2020/1118/171150_05c74562_8032586.png">  
<img src="https://images.gitee.com/uploads/images/2020/1118/171221_db6e83fb_8032586.png">  
<img src="https://images.gitee.com/uploads/images/2020/1118/171400_8b4aea2a_8032586.png">  
&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;选定国家：  
<img src="https://images.gitee.com/uploads/images/2020/1118/171429_3a18ea27_8032586.png">  
<img src="https://images.gitee.com/uploads/images/2020/1118/171452_c99364d2_8032586.png">  
<img src="https://images.gitee.com/uploads/images/2020/1118/171511_0652b50d_8032586.png">  
</div>

## 四、实验要求  
#### 1、实验项目push到自己个人的码云的公开/私有仓库，并把仓库网址登记在在线文档  
#### 2、从码云仓库下载项目的ZIP文件，并交由班长汇总  
#### 3、撰写实验报告，完成各个实验任务。各实验任务中的附图，是老师的演示代码，同学们应该模仿演示代码改为自己的代码。实验报告中必须完整描述各实验任务实现过程并附截图  
#### 4、[网络空间安全学院实验报告模板(2019试行)](https://css.dgut.edu.cn/article?262=)  
#### 5、[实验三项目仓库登记表](https://docs.qq.com/sheet/DUkZyQUhJRnRkd09R?tab=BB08J2)  
#### 6、严禁抄袭。如果不想项目仓库给别人看到，可以设置为私有仓库，并把老师的码云账号(dgsai@vip.qq.com)加入到仓库开发者以便老师检查  