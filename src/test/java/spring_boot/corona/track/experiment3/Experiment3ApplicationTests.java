package spring_boot.corona.track.experiment3;

import com.sun.source.tree.AssertTree;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import spring_boot.corona.track.experiment3.controllers.HomeController;
import spring_boot.corona.track.experiment3.services.GetDataService;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE, classes = {GetDataService.class})
class Experiment3ApplicationTests {

    @Autowired
    GetDataService getDataService;

    @Test
    void testGetDataService() {
        // 测试爬取的数据是否大于100条，大于则爬取成功
        Assertions.assertTrue(getDataService.getAllRegionStats().size() > 100);
    }

}
