package spring_boot.corona.track.experiment3;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import spring_boot.corona.track.experiment3.services.GetDataService;

import java.util.Arrays;

@SpringBootApplication
@EnableScheduling
public class Experiment3Application {

    public static void main(String[] args) {
        SpringApplication.run(Experiment3Application.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext act) {
        return args -> {
            System.out.println("应用程序启动时就获取一次数据！");
            GetDataService getDataService = new GetDataService();
            System.out.println("获得的数据量：" + getDataService.getAllRegionStats().size() + "条");
        };
    }

}
