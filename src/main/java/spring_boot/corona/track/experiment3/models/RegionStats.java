package spring_boot.corona.track.experiment3.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

/**
 * @Author: 王志华
 * @Date: 2020/11/10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegionStats {

    // 省份或州
    private String state;

    // 国家
    private String country;

    // 最新确诊总人数
    private int latestTotalCases;

    // 记录日期
    private List<String> dateList;

    // 每日新增确诊
    private Map<String, Integer> dayAdd;

}
